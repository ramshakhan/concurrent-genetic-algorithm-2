import com.sun.org.apache.bcel.internal.generic.POP;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;

public class Population {

    private ArrayList<Individual> individuals = new ArrayList<>();
    public ArrayList<Individual> newIndividuals = new ArrayList<>();
    private final int POPULATION_SIZE = 300;
    private final int INDIVIDUAL_SIZE = 100;
    private AlgorithmSelector algorithmSelector;
    private Generation generation;
    public boolean flag = false;

    public Population(ArrayList<Individual> individuals, AlgorithmSelector algorithmSelector, Generation generation) {
        this.individuals = individuals;
        this.algorithmSelector = algorithmSelector;
        this.generation = generation;
    }

    // Print out the Maximum Fitness of the population
    public double maxPopulation() {
        double max = individuals.get(0).individualEvaluation();
        for (int i = 1; i < individuals.size(); i++) {
            if (individuals.get(i).individualEvaluation() > max) {
                max = individuals.get(i).individualEvaluation();
            }
        }
        return max;
    }

    // Print out the Minimum Fitness of the population
    public double minPopulation() {
        double min = individuals.get(0).individualEvaluation();
        for (int i = 0; i < individuals.size(); i++) {
            if (individuals.get(i).individualEvaluation() < min) {
                min = individuals.get(i).individualEvaluation();
            }
        }
        return min;
    }

    // Print out the Average Fitness of the population
    public double averagePopulation() {
        double average = 0.0;
        for (int i = 0; i < individuals.size(); i++) {
            average = average + individuals.get(i).individualEvaluation();
        }
        average = average / individuals.size();
        return average;
    }
    
    // putting the parents in blocking queue for crossover process to take the parents
    // storing the children in another blocking queue for the new population task
    // Producer-Consumer Design
    public void populatingNewPopulation(int sizeOfParents, ExecutorService exec) throws InterruptedException {
        int halfPopulationSize = (POPULATION_SIZE / 2);
        BlockingQueue<ArrayList<Individual>> tempParentIndividualQueue = new ArrayBlockingQueue<>(halfPopulationSize);
        SelectParent[] selectParents = new SelectParent[halfPopulationSize];
        CrossOver[] crossOverProcess = new CrossOver[halfPopulationSize];

        // TODO: Synchronization HERE! Separate the For Loop
        for (int i = 0; i < halfPopulationSize; i++) {
            selectParents[i] = new SelectParent(tempParentIndividualQueue, individuals, sizeOfParents, POPULATION_SIZE);
            exec.execute(selectParents[i]);
        }

        for (int i = 0; i < halfPopulationSize; i++) {
            crossOverProcess[i] = new CrossOver(tempParentIndividualQueue.take(), newIndividuals, INDIVIDUAL_SIZE, algorithmSelector);
            exec.execute(crossOverProcess[i]);
        }

        Mutation mutation = new Mutation(newIndividuals, algorithmSelector, INDIVIDUAL_SIZE, POPULATION_SIZE);
        exec.execute(mutation);

        synchronized (newIndividuals) {
            while (true) {
                if (newIndividuals.size() == POPULATION_SIZE && mutation.getCounter() == POPULATION_SIZE) {
                    break;
                } else {
                    newIndividuals.wait();
                }
            }
        }
    }

    // Clears the Individuals Array and add in the New Individuals values in it, then clear the New Individuals Array
    public void moveNewPopulationToOldPopulation() {
        synchronized (newIndividuals) {
            while (true) {
                if (newIndividuals.size() == POPULATION_SIZE) {
                    individuals.clear();
                    individuals.addAll(newIndividuals);
                    newIndividuals.clear();
                    flag = true;

                    int generationCounter = generation.getGenerationCounter();
                    generation.setGenerationCounter(generationCounter + 1);
                    break;
                } else {
                    System.out.println(newIndividuals.size());
                }
            }
        }
    }
}