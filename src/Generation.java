import java.util.*;
import java.util.concurrent.ExecutorService;

public class Generation {

    private double[] minFitnessArray;
    private int generationCounter = 0;

    public void runGenerations(ExecutorService exec) throws InterruptedException {
        Random random = new Random();
        Scanner input = new Scanner(System.in);

        final int POPULATION_SIZE = 300;
        final int INDIVIDUAL_SIZE = 100;
        double[][] array = new double[POPULATION_SIZE][INDIVIDUAL_SIZE];

        // Ask input from user for Number of Generations needed to be iterated
        // Ask input from user for Size of Parents that will be compared in the Tournament Selection Method
        // Ask input from user for the type of Algorithm used
        System.out.print("Number of Generations: ");
        int numberOfGenerations = input.nextInt();
        System.out.print("Size of Parents: ");
        int sizeOfParents = input.nextInt();
        System.out.println("== List of Algorithm == ");
        System.out.println("1 - Schwefel\n2 - Rosenbrock\n3 - Rastrigin\n4 - Simple");
        System.out.print("Algorithm: ");
        int chooseAlgorithm = input.nextInt();
        System.out.println();

        AlgorithmSelector algorithmSelector = new AlgorithmSelector(chooseAlgorithm);
        double max = algorithmSelector.getMax();
        double min = algorithmSelector.getMin();

        // Keep the Minimum Fitness for plotting into the graph
        minFitnessArray = new double[numberOfGenerations];

        // Initialize the array based on random numbers
        for (int i = 0; i < POPULATION_SIZE; i++) {
            for (int j = 0; j < INDIVIDUAL_SIZE; j++) {
                array[i][j] = (random.nextDouble() * (max - min)) + min;
            }
        }

        // Adding each of rows in the array into an ArrayList
        ArrayList<Individual> individuals = new ArrayList<>();

        for (int i = 0; i < POPULATION_SIZE; i++) {
            individuals.add(new Individual(array[i], chooseAlgorithm));
        }

        Population population = new Population(individuals, algorithmSelector, this);

        System.out.println("First Generation (Untouched)");
        System.out.println("Maximum: " + population.maxPopulation());
        System.out.println("Minimum: " + population.minPopulation());
        System.out.println("Average: " + population.averagePopulation());
        System.out.println();

        // Start the Timer
        long startTime = System.currentTimeMillis();

        // The Main Process that iterates the Generations
        while (generationCounter < numberOfGenerations) {
            population.populatingNewPopulation(sizeOfParents, exec);

            // Start the Mutation task by taking the children from the
            population.moveNewPopulationToOldPopulation();

            // Keep the Minimum Fitness value for Graph Plotting
            minFitnessArray[generationCounter - 1] = population.minPopulation();

            // Print Max, Min and Avg for every Generation of the Population
            System.out.println(generationCounter + " Generation");
            System.out.println("Maximum: " + population.maxPopulation());
            System.out.println("Minimum: " + minFitnessArray[generationCounter - 1]);
            System.out.println("Average: " + population.averagePopulation());
            System.out.println();
        }

        // End the Timer
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Total Time is: " + totalTime + "ms");
    }

    public int getGenerationCounter() {
        return generationCounter;
    }

    public void setGenerationCounter(int generationCounter) {
        this.generationCounter = generationCounter;
    }

    public double[] getFitnessArray() {
        return minFitnessArray;
    }
}