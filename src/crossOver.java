import java.util.ArrayList;
import java.util.Random;

public class CrossOver implements Runnable {
    private ArrayList<Individual> tempParentArray;
    private int individualSize;
    private AlgorithmSelector algorithmSelector;
    private ArrayList<Individual> newIndividuals;
    private Random random = new Random();

    public CrossOver(ArrayList<Individual> tempParentArray, ArrayList<Individual> newIndividuals, int individualSize, AlgorithmSelector algorithmSelector) {
        this.tempParentArray = tempParentArray;
        this.individualSize = individualSize;
        this.algorithmSelector = algorithmSelector;
        this.newIndividuals = newIndividuals;
    }

    @Override
    public void run() {
        ArrayList<Individual> tempChildArray = new ArrayList<>();
        
        Individual parent1 = tempParentArray.get(0);
        Individual parent2 = tempParentArray.get(1);
        
        // First Child
        double child1[] = new double[individualSize];
        double chance = random.nextDouble();
        int locus = random.nextInt(individualSize);
        if (chance <= 0.7) { // 70% Chance to Crossover
            for (int i = 0; i <= locus; i++) {
                child1[i] = parent1.getChromosome()[i];
            }
            for (int i = locus; i < individualSize; i++) {
                child1[i] = parent2.getChromosome()[i];
            }
            tempChildArray.add(new Individual(child1, algorithmSelector.getSelector()));
        } else { // 30% Chance to not Crossover
            tempChildArray.add(parent1);
        }

        // Second Child
        double child2[] = new double[individualSize];
        chance = random.nextDouble();
        locus = random.nextInt(individualSize);
        if (chance <= 0.7) { // 70% Chance to Crossover
            for (int i = 0; i <= locus; i++) {
                child2[i] = parent2.getChromosome()[i];
            }
            for (int i = locus; i < individualSize; i++) {
                child2[i] = parent1.getChromosome()[i];
            }
            tempChildArray.add(new Individual(child2, algorithmSelector.getSelector()));
        } else { // 30% Chance to not Crossover
            tempChildArray.add(parent2);
        }

        synchronized (newIndividuals) {
            newIndividuals.add(tempChildArray.get(0));
            newIndividuals.add(tempChildArray.get(1));
            newIndividuals.notifyAll();
        }
    }
}
